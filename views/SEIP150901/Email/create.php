<?php
require_once("../../../vendor/autoload.php");
use App\Message\Message;
if(!isset($_SESSION))session_start();
echo Message::getMessage();
?>
<!DOCTYPE html>
<html>
<head>
    <title>Email</title>
    <meta name="robots" content="noindex, nofollow">
    <!-- Include CSS File Here -->
    <link rel="stylesheet" href="../../../Resource/assets_email/css/style.css"/>
    <link rel="stylesheet" href="../../../Resource/font-awesome/css/font-awesome.min.css">
    <!-- Include CSS File Here -->
    <script src="../../../Resource/assets_email/js/jquery.min.js"></script>
    <!--<script type="text/javascript" src="../../../Resource/assets_email/js/login.js"></script>-->
</head>
<body>
<div class="container" >
    <h2>Add Email</h2>
    <div class="main">
        <form role="form" action="store.php" method="post" class="login-form">

            <div class="form-group"><i class="fa fa-user"></i>
            <label>Username :</label>
            <input type="text" name="username" id="user">
            </div>
            <div class="form-group"><i class="fa fa-inbox"></i>
            <label>Email :</label>
            <input type="email" name="email" id="email">
                </div>
            <button type="submit" class="btn">Create</button>
        </form>
    </div>
</div>
</body>
</html>