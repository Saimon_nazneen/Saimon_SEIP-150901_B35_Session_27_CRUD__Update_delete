<?php
require_once("../../../vendor/autoload.php");
use App\Message\Message;
if(!isset($_SESSION))session_start();
echo Message::getMessage();
?>

<!DOCTYPE html>
<html>
<head>
    <title>Gender</title>
    <meta name="robots" content="noindex, nofollow">
    <!-- Include CSS File Here -->
    <link rel="stylesheet" href="../../../Resource/assets_gender/css/style.css"/>
    <link rel="stylesheet" href="../../../Resource/font-awesome/css/font-awesome.min.css">
    <!-- Include JavaScript File Here -->
    <script src="../../../Resource/assets_gender/js/jquery.min.js"></script>
    <script type="text/javascript" src="../../../Resource/assets_gender/js/reset.js"></script>
</head>
<body>
<div class="container">
    <h1>Add Gender</h1>
    <div class="main">

        <form role="form" action="store.php" method="post" class="login-form">
            <div class="form-group"> <i class="fa fa-users"></i>
            <label>Name :</label>
            <input type="text" name="name" />
            </div>
            <div class="form-group"> <i class="fa fa-gender"></i>
            <label>Gender :</label>
                <div class="form-group">
            <input type="radio" name="sex" value="male">Male<br></div>
                <div class="form-group">
            <input type="radio" name="sex" value="female">Female</div>
            </div><br>
            <div class="form-group">
                <button type="submit" class="btn">Create</button>
                </div>
        </form>
    </div>
</div>
</body>
</html>