<?php
require_once("../../../vendor/autoload.php");
use App\Message\Message;
if(!isset($_SESSION))session_start();
echo Message::getMessage();

use App\BookTitle\BookTitle;

$objBookTitle=new BookTitle();
$objBookTitle->setData($_GET);
$oneData=$objBookTitle->view("obj");



?>



<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>BookTitle_Edit</title>

    <!-- CSS -->
    <link rel="stylesheet" href="../../../Resource/assets_book/bootstrap/css/">
    <link rel="stylesheet" href="../../../Resource/assets_book/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../Resource/assets_book/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../../Resource/assets_book/css/form-elements.css">
    <link rel="stylesheet" href="../../../Resource/assets_book/css/style.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="../../../Resource/assets_book/js/html5shiv.js"></script>
    <script src="../../../Resource/assets_book/js/respond.min.js"></script>
    <![endif]-->


</head>

<body>


<div class="top-content">
    <div class ="container">
        <div class="col-sm-5 col-sm-offset-3 form-box">
            <div class="form-top">
                <div class="form-top-left">
                    <h3>Edit_BookTitle</h3>
                    <p>Edit BookTitle and Author name:</p>
                </div>
                <div class="form-top-right">
                    <i class="fa fa-book"></i>
                </div>
            </div>
            <div class="form-bottom">
                <form role="form" action="update.php" method="post" class="login-form">

                    <input type ="hidden" name="id" value="<?php echo $oneData->id?>" >
                    <div class="form-group">
                        <label class="sr-only" for=Book_Title">Book_Title</label>
                        <input type="text" name="book_title" value="<?php echo $oneData->book_title ?>" class="Book_Title form-control" id="book_Title">
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="author_name">Author_name</label>
                        <input type="text" name="author_name" value="<?php echo $oneData->author_name ?>" class="author_name form-control" id="author_name">
                    </div>
                    <button type="submit" class="btn">Update</button>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Javascript -->
<script src="../../../Resource/assets_book/js/jquery-1.11.1.min.js"></script>
<script src="../../../Resource/assets_book/bootstrap/js/bootstrap.min.js"></script>
<script src="../../../Resource/assets_book/js/jquery.backstretch.min.js"></script>
<script src="../../../Resource/assets_book/js/scripts.js"></script>


</body>

</html>