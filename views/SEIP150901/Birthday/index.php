<head>
    <link rel="stylesheet" href="../../../Resource/assets_book/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../Resource/assets_book/font-awesome/css/font-awesome.min.css"
    <script src="../../../Resource/assets_book/js/jquery-1.11.1.min.js"></script>
    <script src="../../../Resource/assets_book/bootstrap/js/bootstrap.min.js"></script>
    <img src="../../../Resource/assets_book/img/backgrounds/1@2x.jpg" style="position: absolute;
    margin: 0px; padding: 0px;border: medium none; width: 100%; height: 100%;
    z-index: -999999; top: 0px;" >

</head>
<style>
    table {
        border-spacing: 0;
        border-collapse: collapse;
        Margin: 50px auto;
        background-color: rgba(11, 42, 68, 0.09);
    }
    td, tr {
        padding: 8px;
        text-align: center;
    }
    th {
        text-align: center;
        padding: 25px;
    }
</style>

<?php
require_once("../../../vendor/autoload.php");

use App\Birthday\Birthday;
use App\Message\Message;
$objBirthday = new Birthday();
$allData = $objBirthday -> index("obj");
$serial = 1;

echo "<table > ";
echo "<th> Serial </th> <th> ID </th> <th> username </th> <th> Birthdate </th><th> Action </th>";
foreach($allData as $oneData){
    echo "<tr>";
    echo "<td> $serial </td>";
    echo "<td> $oneData->id </td>";
    echo "<td> $oneData->person_name </td>";
    echo "<td> $oneData->birthdate </td>";

    echo
        "<td>
          <a href='view.php? id=$oneData->id'><button class='btn btn-info'>View </button></a>
          <a href='edit.php? id=$oneData->id'><button class='btn btn-success'>Edit </button></a>
          <a href='delete.php? id=$oneData->id'><button class='btn btn-danger'>Delete </button></a>
        </td>
        ";



    echo "</tr>";
    $serial++;
}

echo "</table>";

