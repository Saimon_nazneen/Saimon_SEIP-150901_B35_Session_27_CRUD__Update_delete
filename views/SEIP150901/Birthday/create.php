<?php
require_once("../../../vendor/autoload.php");
use App\Message\Message;
if(!isset($_SESSION))session_start();
echo Message::getMessage();
?>


<!DOCTYPE html>
<html>
<head>
    <title>Birthday</title>
    <meta name="robots" content="noindex, nofollow"/>
    <!------------ Including jQuery Date UI with CSS -------------->
    <script src="../../../Resource/assets_birth/js/jquery-1.10.2.js"></script>
    <script src="../../../Resource/assets_birth/js/jquery-ui.js"></script>
    <link rel="stylesheet" href="../../../Resource/assets_birth/css/jquery-ui.css">
    <!-- jQuery Code executes on Date Format option ----->
    <script src="../../../Resource/assets_birth/js/script.js"></script>
    <link rel="stylesheet" href="../../../Resource/assets_birth/css/style.css">
    <link rel="stylesheet" href="../../../Resource/font-awesome/css/font-awesome.min.css">
</head>
<body>
<div class="container">
    <div class="main">
    <h2>Birthday</h2>
    <div >
        <form action="store.php" method="post">
            <label>Name :</label>
            <input type="text" name="person_name" id="Name"/>
            <label>Date Of Birth :</label>
            <input type="date"  name="birthdate" />

            <input type="submit" id="submit" value="Create">
        </form>
    </div>
        </div>
</div>

</body>
</html>
