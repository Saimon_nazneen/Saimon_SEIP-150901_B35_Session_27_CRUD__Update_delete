<?php
require_once("../../../vendor/autoload.php");
use App\Message\Message;
if(!isset($_SESSION))session_start();
echo Message::getMessage();
?>
<!DOCTYPE html>
<html>
<head>
    <title>City</title>
    <meta name="robots" content="noindex, nofollow">
    <!-- Include CSS File Here -->
    <link rel="stylesheet" href="../../../Resource/assets_city/css/select_jquery.css"/>
    <link rel="stylesheet" href="../../../Resource/font-awesome/css/font-awesome.min.css">
    <!-- Include JS File Here -->
    <script src="../../../Resource/assets_city/js/jquery.min.js"></script>
    <script type="text/javascript" src="../../../Resource/assets_city/js/select_jquery.js"></script>
</head>
<body>
<div class="container">

    <div class="main">
        <h2>City</h2>
        <form action="store.php" method="post">

        <label>Select Country:</label>
        <div id="prm">
            <select id="country" name="country_name">
                <option>--Select--</option>
                <option>USA</option>
                <option>AUSTRALIA</option>
                <option>FRANCE</option>
                <option>Bangladesh</option>
            </select>
            <br><br>
            <label>Select City:</label>
            <select id="city" name="city_name">
                <!-- Dependent Select option field -->
            </select><br>
        </div>
        <button type="submit" class="btn">Create</button>
        </form>

    </div>
</div>
</body>
</html>
