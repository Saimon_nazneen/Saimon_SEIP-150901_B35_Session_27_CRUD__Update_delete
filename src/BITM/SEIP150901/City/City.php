<?php
namespace App\City;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;

class City extends DB{
    public $id;
    public $country_name;
    public $city_name;

    public function __construct()
    {
        parent:: __construct();

    }

    public function setData($postVariableData = NULL)
    {
        if (array_key_exists('id',$postVariableData)) {
            $this->id = $postVariableData['id'];
        }
        if (array_key_exists('country_name',$postVariableData)) {
            $this->country_name = $postVariableData['country_name'];
        }

        if (array_key_exists('city_name',$postVariableData)) {
            $this->city_name = $postVariableData['city_name'];
        }

    }

    public function store(){

        $arrData = array($this->country_name, $this->city_name);
        $sql = "Insert INTO city(country_name,city_name) VALUES (?,?)";

        $STH = $this->DBH->prepare($sql);

        $result = $STH->execute($arrData);
        if($result)
            Message::setMessage("DATA has been Inserted Successfully :)");
        else
            Message:: setMessage("Failed! DATA has not been Inserted succecssfully :(");
        Utility::redirect('create.php');

    }// end of store method

}



