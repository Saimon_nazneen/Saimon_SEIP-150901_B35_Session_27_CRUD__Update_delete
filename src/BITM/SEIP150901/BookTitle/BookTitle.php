<?php
namespace App\BookTitle;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;
use PDO;

class BookTitle extends DB
{
    public $id;
    public $book_title;
    public $author_name;

    public function __construct()
    {
        parent:: __construct();
    }

    public function setData($postVariableData = NULL)
    {
        if (array_key_exists('id',$postVariableData)) {
            $this->id = $postVariableData['id'];
        }
        if (array_key_exists('book_title',$postVariableData)) {
            $this->book_title = $postVariableData['book_title'];
        }
        if (array_key_exists('author_name',$postVariableData)) {
            $this->author_name = $postVariableData['author_name'];
        }
    }

    public function store(){
        $arrData = array($this->book_title, $this->author_name);
        $sql = "Insert INTO book_title(book_title, author_name) VALUES (?,?)";

        $STH = $this->DBH->prepare($sql);

        $result = $STH->execute($arrData);
        if($result)
            Message::setMessage("DATA has been Inserted Successfully :)");
        else
            Message:: setMessage("Failed! DATA has not been Inserted succecssfully :(");
        Utility::redirect('create.php');

    }// end of store method


    public function index()
    {              //ASSOC = Associative array

        $STH = $this->DBH->query('SELECT * from book_title');
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $arrAllData = $STH->fetchAll();
        return $arrAllData;
    }


    /*
    or, optional;
    public function index($fetchMode='ASSOC'){              //ASSOC = Associative array

        $STH = $this->DBH->query('SELECT * from book_title');

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;

    }// end of index();

    */

    public function view($fetchMode='ASSOC'){              //ASSOC = Associative array

        $STH = $this->DBH->query('SELECT * from book_title WHERE id='.$this->id);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrOneData  = $STH->fetch();
        return $arrOneData;

    }// end of index();



public function update()
{
    $arrData = array($this->book_title, $this->author_name);
    $sql = "UPDATE book_title SET book_title = ?, author_name =? WHERE id =".$this->id;
    $STH=$this->DBH->prepare($sql);
    $STH->execute($arrData);
    Utility::redirect('index.php');

}//end of update method;

public function delete()
{
    $sql= "DELETE FROM book_title WHERE id=".$this->id;

    $STH = $this->DBH->prepare($sql);
    $STH->execute();
    Utility::redirect('index.php');
}// end of delete;


    public function trash()
    {


    }

}// end of class