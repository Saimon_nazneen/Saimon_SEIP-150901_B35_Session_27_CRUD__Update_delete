<?php
namespace App\SummaryOfOrganization;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;

class SummaryOfOrganization extends DB{
    public $id;
    public $org_name	;
    public $org_summary;

    public function __construct()
    {
        parent:: __construct();

    }

    public function setData($postVariableData = NULL)
    {
        if (array_key_exists('id',$postVariableData)) {
            $this->id = $postVariableData['id'];
        }
        if (array_key_exists('org_name',$postVariableData)) {
            $this->org_name = $postVariableData['org_name'];
        }
        if (array_key_exists('org_summary',$postVariableData)) {
            $this->org_summary = $postVariableData['org_summary'];
        }
    }

    public function store(){

        $arrData = array($this->org_name, $this->org_summary);
        $sql = "Insert INTO summaryoforganization(org_name, org_summary) VALUES (?,?)";

        $STH = $this->DBH->prepare($sql);

        $result = $STH->execute($arrData);
        if($result)
            Message::setMessage("DATA has been Inserted Successfully :)");
        else
            Message:: setMessage("Failed! DATA has not been Inserted succecssfully :(");
        Utility::redirect('create.php');

    }// end of store method

}



