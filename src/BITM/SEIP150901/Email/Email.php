<?php
namespace App\Email;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;

class Email extends DB
{
    public $id;
    public $username;
    public $email;

    public function __construct()
    {
        parent:: __construct();

    }

    public function setData($postVariableData = NULL)
    {
        if (array_key_exists('id',$postVariableData)) {
            $this->id = $postVariableData['id'];
        }
        if (array_key_exists('username',$postVariableData)) {
            $this->username = $postVariableData['username'];
        }
        if (array_key_exists('email',$postVariableData)) {
            $this->email = $postVariableData['email'];
        }
    }

    public function store(){

        $arrData = array($this->username, $this->email);
        $sql = "Insert INTO email_1(username, email) VALUES (?,?)";

        $STH = $this->DBH->prepare($sql);

        $result = $STH->execute($arrData);
        if($result)
            Message::setMessage("DATA has been Inserted Successfully :)");
        else
            Message:: setMessage("Failed! DATA has not been Inserted succecssfully :(");
        Utility::redirect('create.php');

    }// end of store method

}



